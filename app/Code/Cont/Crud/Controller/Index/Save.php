<?php
namespace Cont\Crud\Controller\Index;
use Magento\Framework\Controller\ResultFactory;
class Save extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_postFactory;
    public function __construct(
            \Magento\Framework\App\Action\Context $context,
            \Magento\Framework\View\Result\PageFactory $pageFactory,
            \Cont\Crud\Model\PostFactory $postFactory
            )
    {
        $this->_pageFactory = $pageFactory;
        $this->_postFactory = $postFactory;    
        return parent::__construct($context);
    }
    
    public function execute()
    {
        /*$post = $this->_postFactory->create();
        $collection = $post->getCollection();
        foreach($collection as $item){
                echo "<pre>"; print_r($item->getData()); echo "</pre>";
        }*/
        /*exit();
        return $this->_pageFactory->create();
        */
        $model = $this->_postFactory->create();
        $data = (array) $this->getRequest()->getPost();
        if (!empty($data)) {
            $name    = $data['name'];
            $ct     = $data['ctime'];
            $ut = $data['utime'];
	    $id      = $data['id'];
			
            /*$model->loadData('name', $name);
            $model->loadData('url_key', $url);
            $model->loadData('post_content', $content);
            $model->loadData('status', $status);
			$model->save();*/
			
            $model->load($id);
            $model->setName($name)->setCreated_at($ct)->setUpdated_at($ut)->setId($id);
            $model->save();
			
             // now deprecated, but works fine if you have no repository
            /*$resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($data['fname']);
            */
            
            $this->messageManager->addSuccessMessage('Record updated !');
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl('/magento_harish/crud/index/index');
            
            //return $resultJson;
            
            
            return $resultRedirect;
        }
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}