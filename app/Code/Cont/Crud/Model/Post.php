<?php
namespace Cont\Crud\Model;
class Post extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'cont_crud_post';

	protected $_cacheTag = 'cont_crud_post';

	protected $_eventPrefix = 'cont_crud_post';

	protected function _construct()
	{
		$this->_init('cont\crud\Model\ResourceModel\Post');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}