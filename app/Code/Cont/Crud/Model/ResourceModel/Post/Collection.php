<?php
namespace Cont\Crud\Model\ResourceModel\Post;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{ 
        protected function _construct()
        {
                $this->_init('Cont\Crud\Model\Post', 'Cont\Crud\Model\ResourceModel\Post');
        }
}