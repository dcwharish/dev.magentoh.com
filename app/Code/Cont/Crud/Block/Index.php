<?php
namespace Cont\Crud\Block;
use Magento\Framework\App\Filesystem\DirectoryList;
 
class Index extends \Magento\Framework\View\Element\Template
{
	protected $_filesystem;
 
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Cont\Crud\Model\PostFactory $postFactory
		)
	{			
		parent::__construct($context);
		$this->_postFactory = $postFactory;
	}	
         public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Custom Module List Page'));
        return parent::_prepareLayout();
    }
	public function getResult()
	{
		$post = $this->_postFactory->create();
		$collection = $post->getCollection();
		return $collection;
	}
}